﻿using System;
using System.Collections.Generic;

namespace AvoidObstacles
{
    class Program
    {
        static void Main(string[] args)
        {
            // System.Console.WriteLine(999 % 6);
            // System.Console.WriteLine(1000 % 6);

            int[] arr = new int[]{5, 3, 6, 7, 9};
            System.Console.WriteLine(avoidObstacles(arr));
            System.Console.WriteLine(avoidObstacles(new int[] {5, 8, 9, 13, 14}));
            System.Console.WriteLine(avoidObstacles(new int[] {2, 3}));
            System.Console.WriteLine(avoidObstacles(new int[]{19, 32, 11, 23}));

            System.Console.WriteLine(avoidObstacles(new int[]{1, 4, 10, 6, 2}));
            System.Console.WriteLine(avoidObstacles(new int[]{1000, 999}));


        }

        static int avoidObstacles(int[] inputArray) {
            
            int maxValue = 0;
            
            foreach (var item in inputArray)
            {
                if(item > maxValue){
                    maxValue = item;
                }
            }
            
            for(int i = 2; i < maxValue + 10; i++){
                
                List<bool> trueForAll = new List<bool>();
                foreach(int item in inputArray){
                    if(item % i != 0){
                        trueForAll.Add(true);
                    }else{
                        trueForAll.Add(false);
                    }
                }
                
                if(trueForAll.TrueForAll(y =>  y == true)){
                return i;
                }
            }
            return 0;
            
            
        }

    }
}
